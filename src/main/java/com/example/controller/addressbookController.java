package com.example.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.example.service.addressbookService;

@Controller

public class addressbookController {
	//inject dependency i.e addressbook service
	private  addressbookService addressService;

	public addressbookController(addressbookService addressService) {
		super();
		this.addressService = addressService;
	}
	//create handler method to handle the list of addressbook details
	//and return view
	//mapp the table i.e in this case my table name is details.
	@GetMapping("/details")
	public String listDetails(Model model) {
		model.addAttribute("details", addressService.getAlladdressbookEntity());
		//this returns the view and the view is details.
		return "details";
		//after this we create a view for the address book details in the template folder to which is going to be in a table form.
		
		
	}

}
