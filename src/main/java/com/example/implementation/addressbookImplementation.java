package com.example.implementation;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.entity.addressbookEntity;
import com.example.repository.addresbookRepository;
import com.example.service.addressbookService;
@Service
public class addressbookImplementation implements addressbookService{

	//after implementing the unimplemented method the inject the repository dependency here.
	private addresbookRepository addresrepository;
	
	public addressbookImplementation(addresbookRepository addresrepository) {
		super();
		this.addresrepository = addresrepository;
	}




	@Override
	public List<addressbookEntity> getAlladdressbookEntity() {
		//call final method which will return the blist of all address details.
		return addresrepository.findAll();
	}

}
